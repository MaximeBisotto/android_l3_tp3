package com.example.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.myapplication.CityActivity;
import com.example.myapplication.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listCity = findViewById(R.id.list_city);
        final WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, dbHelper.fetchAllCities(), new String[] {WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY}, new int[] {android.R.id.text1, android.R.id.text2});
        listCity.setAdapter(cursorAdapter);
        listCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                City city = dbHelper.cursorToCity( (Cursor) parent.getItemAtPosition(position));
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City.TAG, city);
                startActivity(intent);
            }
        });

        listCity.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//                menu.add(R.menu.layout);
                getMenuInflater().inflate(R.menu.row, menu);
                MenuItem supprimer = (MenuItem) findViewById(R.id.item_suppression);
            }
        });

    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        AdapterView adapterView = (AdapterView) findViewById(R.id.list_city);
        Cursor cursor = (Cursor) adapterView.getItemAtPosition(info.position);
        WeatherDbHelper dbHelper = new WeatherDbHelper(MainActivity.this);
        dbHelper.deleteCity(cursor);
        onResume();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        ListView listCity = findViewById(R.id.list_city);
        final WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, dbHelper.fetchAllCities(), new String[] {WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY}, new int[] {android.R.id.text1, android.R.id.text2});
        listCity.setAdapter(cursorAdapter);
    }
}
